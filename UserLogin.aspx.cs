﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Web.Configuration;
using System.Data.OleDb;
using System.Web.Services.Description;
using System.Windows.Forms;
using System.Data;
public partial class UserLogin : Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ArmyTraingConnectionString1"].ConnectionString);
    SqlCommand cmd;
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void LogIn()
        {
            if (con.State == ConnectionState.Closed)
                con.Open();


            cmd = new SqlCommand("[UserLogin]", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LoginName", UserName.Text);
            cmd.Parameters.Add("@Password", SqlDbType.NVarChar, 50).Value = Password.Text;
            cmd.Parameters.Add("@responseMessage", SqlDbType.NVarChar, 150).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@userID", SqlDbType.NVarChar, 14).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@University_ID", SqlDbType.Int, 3).Direction = ParameterDirection.Output;
            // cmd.Parameters.AddWithValue("responseMessage", LblErrMesage.Text);
            int k = cmd.ExecuteNonQuery();
            if (k != 0)
            {
                Session["University_ID"] = Convert.ToString(cmd.Parameters["@University_ID"].Value);
                Session["userID"] = Convert.ToString(cmd.Parameters["@userID"].Value);
                LblErrMesage.Text = Convert.ToString(cmd.Parameters["@responseMessage"].Value);
                con.Close();

            }
        }
        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            LogIn();
            if (string.IsNullOrEmpty(Session["userID"].ToString()))
            { LblErrMesage.Text = Convert.ToString(cmd.Parameters["@responseMessage"].Value); }
            else
            {

                FormsAuthentication.RedirectFromLoginPage(LblErrMesage.Text, true);
                Response.Redirect("UserMain.aspx?UserID=" + LblErrMesage.Text);
            }


        }
}